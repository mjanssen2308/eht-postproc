Repository for tools and metadata for the post-processing of UVFITS files, after correlation and signal-stabilization steps of our calibration pipelines.

The order of the incremental post-processing calibration steps are defined in eht_postproc.py via all_calib_steps. The calibration functions are defined in eht_postproc.py.
In calibration_data.py, the netcal_and_leakage dictionary defines the calibration data: The UVFITS files to be post-processed and for each file, the calibration steps to be executed with the corresponding metadata following the keys defined in all_calib_steps.

Requires Python version > 3.9 and ehtim (https://github.com/achael/eht-imaging).
