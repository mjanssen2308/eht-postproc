#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2023 EHTC
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Runs calibration steps for UVFITS data as specified in an accompanying calibration_data.py file.
Based on Maciek's jupyter notebooks. Must have vex ('v') info in calibration_data.py file.
- Applies EVPA rotation.
- Applies leakage solutions.
- Applies scaling factors for individual antennas.
- Applies netcal (inc. lightcurves for SGRA based on time,amp,sigma csv files).
- Applies self-calibration of LMT to Gaussian sourcesize.
- Averages data in 10s.
- Ensures that the mount types are correctly written in the end (read from initial UVF input files).
"""
import os
import sys
import importlib
import pandas as pd
import numpy as np
import scipy.interpolate as interp
from astropy.io import fits
import ehtim as eh
import ehtim.imaging.dynamical_imaging as di
import ehtim.observing.obs_simulate as simobs
from ehtim.calibrating import polgains_cal as pg

calibration_data_file = sys.argv[1] #python eht_postproc.py calibration_data.py
sys.path.append(os.path.dirname(calibration_data_file))
data = importlib.import_module(os.path.basename(calibration_data_file).replace('.py', ''))


NCPU    = os.cpu_count()
fr_elev = {}
M       = {}
DOBS    = ''
MJD     = 0


def multiproc():
    try:
        Nidle = max(2, NCPU - int(os.getloadavg()[0]))
    except OSError:
        Nidle = 2
    return min(Nidle, NCPU)


def polfixed_save(obs, fname, fname_oldfile=None):
    if not fname_oldfile:
        fname_oldfile = fname
    u = fits.open(fname_oldfile)
    if 'DATE' in u[0].header['PTYPE6']:
        PZ6 = u[0].header['PZERO6']
    else:
        PZ6 = 0
    u.close()
    if not obs:
        obs = eh.obsdata.load_uvfits(fname)
    obs.mjd = MJD
    obs.save_uvfits(fname)
    polfixed_uvload(fname).save_uvfits(fname)
    put_mnts(fname, PZ6)


def polfixed_uvload(uvf):
    old_stdout  = sys.stdout
    mute_stdout = open(os.devnull, 'w')
    sys.stdout  = mute_stdout
    obs         = eh.obsdata.load_uvfits(uvf, polrep='circ')
    sys.stdout  = old_stdout
    ants        = list(obs.tarr['site'])
    for site in ants:
        aindx = ants.index(site)
        obs.tarr['fr_par'][aindx] = 1.0
        if site in fr_elev:
            obs.tarr['fr_elev'][aindx] = fr_elev[site]
    return obs


def get_mnts(_, uvf, *args, **kwargs):
    """
    Load telescope mount types (+DATE-OBS) from initial UVFITS files.
    """
    global fr_elev
    global M
    global DOBS
    global MJD
    u  = fits.open(uvf)
    AN = u['AIPS AN'].data
    for i,a in enumerate(AN['ANNAME']):
        amnt = AN['MNTSTA'][i]
        M[a] = amnt
        if amnt == 4:
            fr_elev[a] = 1.0
        elif amnt == 5:
            fr_elev[a] = -1.0
    DOBS = u[0].header['DATE-OBS']
    u.close()
    MJD = eh.obsdata.load_uvfits(uvf).mjd
    return None, None


def put_mnts(uvf, PZ6):
    """
    Put back initial telescope mount types (+DATE-OBS). In case they were overwritten.
    """
    u = fits.open(uvf, mode='update')
    for i,a in enumerate(u['AIPS AN'].data['ANNAME']):
        u['AIPS AN'].data['MNTSTA'][i] = M[a]
    u[0].header['DATE-OBS'] = DOBS
    if PZ6:
        u[0].header['PZERO6'] = PZ6
    u.flush()
    u.writeto(uvf, overwrite=True)
    u.close()


def netcal_gaintb_extrapolation(gaintb, scans, colocs = ['AA', 'AP', 'JC', 'SM']):
    # TODO: should probably make the array computations faster with numpy
    for st in colocs:
        try:
            _ = gaintb.data[st]
        except KeyError:
            continue
        rgs = []
        lgs = []
        isc = 0
        tsc = scans[isc]
        rpl = []
        for j,x in enumerate(gaintb.data[st]):
            if x[0]+1.e-6 > tsc[0] and x[0]-1.e-6 < tsc[1]:
                ramp = abs(x[1])
                lamp = abs(x[2])
                if round(ramp, 3) + round(lamp, 3) == 2.0:
                    rpl.append(j)
                else:
                    rgs.append(ramp)
                    lgs.append(lamp)
            else:
                isc+= 1
                tsc = scans[isc]
                if not rgs:
                    # No calibration for this scan.
                    rpl = []
                    continue
                rmed = np.max(rgs)
                lmed = np.max(lgs)
                rgs  = []
                lgs  = []
                for r in rpl:
                    gaintb.data[st][r] = (gaintb.data[st][r][0], rmed + 0.j, lmed + 0.j)
                rpl  = []
    return gaintb


def evpa_rot(uvf, evpa, *args, **kwargs):
    """
    Apply an overall EVPA rotation to the data (RL phase of the refant).
    """
    nf = uvf+'+EVPA_rotation'
    if os.path.exists(nf+'.uvfits'):
        print(f'{nf}.uvfits already exists.')
        polfixed_save(None, nf+'.uvfits')
        return nf, None
    obs = polfixed_uvload(uvf)
    obs.add_scans()
    datadict = {t['site']:np.array([evpa], dtype=eh.DTCAL) for t in obs.tarr}
    caltab = eh.caltable.Caltable(obs.ra,obs.dec,obs.rf,obs.bw,datadict,obs.tarr,obs.source,obs.mjd)
    obs = caltab.applycal(obs, interp='nearest', extrapolate=True)
    polfixed_save(obs, nf+'.uvfits', uvf+'.uvfits')
    return nf, None


def dcal(uvf, Dterms, _, vexfile, *args, **kwargs):
    """
    Perform leakage calibration.
    """
    #TODO: It would be nice for apply_jones_inverse() to not flag single-pol data. On e17c07lo, one pol of AP is sometimes flagged upstream.
    nf = uvf+'+dcal'
    if os.path.exists(nf+'.uvfits'):
        print(f'{nf}.uvfits already exists.')
        polfixed_save(None, nf+'.uvfits')
        return nf, None
    obs = polfixed_uvload(uvf+'.uvfits')
    obs.add_scans(info='vex', filepath=vexfile)
    jcmt = 'JC'
    if 'MM' in obs.tarr['site']:
        jcmt = 'MM'
    jrr = obs.data['rrvis'][np.where((obs.data['t1']==jcmt) | (obs.data['t2']==jcmt))]
    jll = obs.data['llvis'][np.where((obs.data['t1']==jcmt) | (obs.data['t2']==jcmt))]
    mpl = None
    if jrr.size / 2 < np.count_nonzero(np.isnan(jrr)):
        mpl = 'r'
    elif jll.size / 2 < np.count_nonzero(np.isnan(jll)):
        mpl = 'l'
    if not mpl:
        for st in Dterms:
            try:
                obs.tarr[obs.tkey[st]]['dr'] = Dterms[st]['dr']
                obs.tarr[obs.tkey[st]]['dl'] = Dterms[st]['dl']
            except KeyError:
                print(f'  Cannot calibrate (station not present): {st} in {uvf}')
        obs.data = simobs.apply_jones_inverse(obs,dcal=False,verbose=False)
        # Set D-terms back to zero
        for st in Dterms:
            try:
                obs.tarr[obs.tkey[st]]['dr'] = 0.+0.j
                obs.tarr[obs.tkey[st]]['dl'] = 0.+0.j
            except KeyError:
                pass
        polfixed_save(obs, nf+'.uvfits', uvf+'.uvfits')
        return nf, None
    obs_JC = obs.flag_sites(jcmt, output='flagged')
    obs_JC0 = obs_JC.copy()
    other_pol = mpl
    if other_pol=='r': present_pol = 'l'
    elif other_pol=='l': present_pol = 'r'
    obs_JC.data[2*other_pol+'vis']=obs_JC.data[2*present_pol+'vis']
    obs_JC.data[2*other_pol+'sigma']=obs_JC.data[2*present_pol+'sigma']
    obs_JC.data[other_pol+present_pol+'vis'] = np.nan_to_num(obs_JC.data[other_pol+present_pol+'vis'],nan = 0)
    obs_JC.data[present_pol+other_pol+'vis'] = np.nan_to_num(obs_JC.data[present_pol+other_pol+'vis'],nan = 0)
    obs_JC.data[other_pol+present_pol+'sigma'] = np.nan_to_num(obs_JC.data[other_pol+present_pol+'sigma'],nan = 0)
    obs_JC.data[present_pol+other_pol+'sigma'] = np.nan_to_num(obs_JC.data[present_pol+other_pol+'sigma'],nan = 0)
    obs_noJC = obs.flag_sites(jcmt,output='kept')
    for st in Dterms:
        if st==jcmt:
            continue
        try:
            obs_noJC.tarr[obs.tkey[st]]['dr'] = Dterms[st]['dr']
            obs_noJC.tarr[obs.tkey[st]]['dl'] = Dterms[st]['dl']
        except KeyError:
            print(f'  Cannot calibrate (station not present): {st} in {uvf}')
    obs_noJC.data = simobs.apply_jones_inverse(obs_noJC,dcal=False,verbose=False)
    # Set D-terms back to zero
    for st in Dterms:
        if st==jcmt:
            continue
        try:
            obs_noJC.tarr[obs.tkey[st]]['dr'] = 0.+0.j
            obs_noJC.tarr[obs.tkey[st]]['dl'] = 0.+0.j
        except KeyError:
            pass
    for st in Dterms:
        try:
            obs_JC.tarr[obs.tkey[st]]['dr'] = Dterms[st]['dr']
            obs_JC.tarr[obs.tkey[st]]['dl'] = Dterms[st]['dl']
        except KeyError:
            print(f'  Cannot calibrate (station not present): {st} in {uvf}')
    obs_JC.data = simobs.apply_jones_inverse(obs_JC,dcal=False,verbose=False)
    # Set D-terms back to zero
    for st in Dterms:
        try:
            obs_JC.tarr[obs.tkey[st]]['dr'] = 0.+0.j
            obs_JC.tarr[obs.tkey[st]]['dl'] = 0.+0.j
        except KeyError:
            pass
    #now JC needs to be masked properly to deal with all singlepol nans
    obs_JC.data[2*other_pol+'vis']=obs_JC0.data[2*other_pol+'vis']
    obs_JC.data[2*other_pol+'sigma']=obs_JC0.data[2*other_pol+'sigma']
    obs_JC.data[other_pol+present_pol+'vis'] *= obs_JC0.data[other_pol+present_pol+'vis']
    obs_JC.data[other_pol+present_pol+'vis'] /= obs_JC0.data[other_pol+present_pol+'vis']
    obs_JC.data[present_pol+other_pol+'vis'] *= obs_JC0.data[present_pol+other_pol+'vis']
    obs_JC.data[present_pol+other_pol+'vis'] /= obs_JC0.data[present_pol+other_pol+'vis']
    obs_JC.data[other_pol+present_pol+'sigma'] *= obs_JC0.data[other_pol+present_pol+'sigma']
    obs_JC.data[other_pol+present_pol+'sigma'] /= obs_JC0.data[other_pol+present_pol+'sigma']
    obs_JC.data[present_pol+other_pol+'sigma'] *= obs_JC0.data[present_pol+other_pol+'sigma']
    obs_JC.data[present_pol+other_pol+'sigma'] /= obs_JC0.data[present_pol+other_pol+'sigma']
    obs_final = eh.merge_obs([obs_noJC,obs_JC])
    obs_final.scans=[]
    obs_final.add_scans(info='vex', filepath=vexfile)
    polfixed_save(obs_final, nf+'.uvfits', uvf+'.uvfits')
    return nf, None


def scale_site(obs_in, site, factor):
    obs_with_site = obs_in.flag_sites([site],output='flagged')
    obs_without_site = obs_in.flag_sites([site],output='kept')
    if obs_in.polrep=='stokes':
        obs_with_site.data['vis']*=factor
        obs_with_site.data['vvis']*=factor
        obs_with_site.data['sigma']*=factor
        obs_with_site.data['vsigma']*=factor
    else:
        obs_with_site.data['rrvis']*=factor
        obs_with_site.data['llvis']*=factor
        obs_with_site.data['rrsigma']*=factor
        obs_with_site.data['llsigma']*=factor
    obs_with_site.scans=[]
    obs_without_site.scans=[]
    obs_out = eh.merge_obs([obs_with_site,obs_without_site])
    return obs_out


def ampscale(uvf, site_dict, *args, **kwargs):
    nf = uvf+'+ampscale'
    if os.path.exists(nf+'.uvfits'):
        print(f'{nf}.uvfits already exists.')
        polfixed_save(None, nf+'.uvfits')
        return nf, None
    obs = polfixed_uvload(uvf+'.uvfits')
    obs.add_scans()
    for s in site_dict:
        obs = scale_site(obs, s, site_dict[s])
    polfixed_save(obs, nf+'.uvfits', uvf+'.uvfits')
    return nf, None



def polgainscal(uvf, *args, **kwargs):
    """
    Apply RR/LL polarimetric gains ratio calibration - required for HOPS data.
    """
    nf = uvf+'+polcal'
    if os.path.exists(nf+'.uvfits'):
        print(f'{nf}.uvfits already exists.')
        polfixed_save(None, nf+'.uvfits')
        return nf, None

    # read uvfits file
    obs = eh.obsdata.load_uvfits(uvf+'.uvfits',polrep='circ')
    obs.scans=[]
    obs.add_scans()

    # select most suitable reference antenna

    ants = ['AA','LM','NN','PV']

    for ant in ants:
        if ant in obs.tarr['site']:
            refant = ant
            break

    print('Performing polarimetric gains ratio calibration on '+uvf+'.uvfits \n')
    print('Using '+refant+' as reference antenna')  
    obscal=pg.polgains_cal(obs,reference=refant,method='phase',pad_amp=0.03,gain_tol=0.2,
                            scan_solutions=True,show_solution=False)
    obscal=pg.polgains_cal(obscal,reference=refant,method='phase',pad_amp=0.03,gain_tol=0.2,
                            scan_solutions=True,show_solution=False)
    obscalout=pg.polgains_cal(obscal,reference=refant,method='amp',pad_amp=0.03,gain_tol=0.2,scan_solutions=True)
    polfixed_save(obscalout, nf+'.uvfits', uvf+'.uvfits')
    return nf, None


def netcal(uvf, zbl_fluxdensity, *args, **kwargs):
    """
    Perform network calibration.
    """
    nf = uvf+'+netcal'
    if os.path.exists(nf+'.uvfits'):
        print(f'{nf}.uvfits already exists.')
        polfixed_save(None, nf+'.uvfits')
        try:
            _ = float(zbl_fluxdensity)
            return nf, None
        except TypeError:
            obs = polfixed_uvload(uvf+'.uvfits')
            obs.add_scans()
            _, _spl = netcal_lightcurve(obs, dict(zbl_fluxdensity), no_cal=True)
            return nf, _spl
    obs = polfixed_uvload(uvf+'.uvfits')
    obs.add_scans()
    sma  = 'SM'
    apex = 'AP'
    jcmt = 'JC'
    if 'SW' in obs.tarr['site']:
        sma = 'SW'
    if 'AX' in obs.tarr['site']:
        apex = 'AX'
    if 'MM' in obs.tarr['site']:
        jcmt = 'MM'
    #obs = obs.flag_anomalous(field='amp',robust_nsigma_cut=4)
    try:
        const = float(zbl_fluxdensity)
        caltb = eh.netcal(obs, const, pol='RRLL', gain_tol=.5, solution_interval=10.0,
                          processes=multiproc(), pad_amp=0.1, caltable=True
                         )
        caltb = netcal_gaintb_extrapolation(caltb, obs.scans, colocs = ['AA', apex, sma, jcmt])
        obs   = caltb.applycal(obs, interp='nearest', extrapolate=True)
        _spl  = const
    except TypeError:
        obs, _spl = netcal_lightcurve(obs, dict(zbl_fluxdensity))
    polfixed_save(obs, nf+'.uvfits', uvf+'.uvfits')
    return nf, _spl


def netcal_lightcurve(obs, ALMA_SMA_LCs, no_cal=False, *args, **kwargs):
    """
    Netcal for ehtim obs data object with light curve measurements of the SMA and ALMA.
    """
    sys_err          = 0.05
    smoothing_factor = 0.9

    sma  = 'SM'
    apex = 'AP'
    jcmt = 'JC'
    if 'SW' in obs.tarr['site']:
        sma = 'SW'
    if 'AX' in obs.tarr['site']:
        apex = 'AX'
    if 'MM' in obs.tarr['site']:
        jcmt = 'MM'
    AL = pd.read_csv(ALMA_SMA_LCs['AA'])
    SL = pd.read_csv(ALMA_SMA_LCs[sma])

    # Scale SMA to ALMA and merge
    overlap_max = AL['time'].max()
    overlap_min = SL['time'].min()
    ALMA_median_overlap = np.median(AL[(AL.time<=overlap_max)&(AL.time>=overlap_min)].amp)
    SMA_median_overlap = np.median(SL[(SL.time<=overlap_max)&(SL.time>=overlap_min)].amp)
    ratio = np.around(ALMA_median_overlap/SMA_median_overlap,3)
    SL['amp'] *= ratio
    LC = pd.concat([AL,SL],ignore_index=True)
    LC.sort_values('time',inplace=True)
    LC = LC.reset_index()
    LC['full_sigma'] = np.sqrt(LC['sigma']**2 + (sys_err*LC['amp'])**2)
    spl = interp.UnivariateSpline(LC['time'], LC['amp'],w=1.+0*(1./LC['full_sigma'])**2, ext=3, k=3)
    spl.set_smoothing_factor(smoothing_factor)
    if no_cal:
        return None, spl

    #obs= obs.flag_anomalous(field='snr',robust_nsigma_cut=4)
    obs= eh.netcal(obs, spl, sites=[apex, sma, jcmt], processes=multiproc(), gain_tol=0.5,
                   pol='RRLL', pad_amp=0.1
                  )
    for _ in range(2):
        obs= eh.netcal(obs, spl, processes=multiproc(), gain_tol=0.5, pol='RRLL', pad_amp=0.1)
    #obs = obs.flag_anomalous(field='amp',robust_nsigma_cut=4)
    #obs = obs.flag_anomalous(field='snr',robust_nsigma_cut=4)
    return obs, spl


def lmtcal(uvf, sourcesize, zbl_spline, vexfile, *args, **kwargs):
    if not zbl_spline:
        print('Cannot get zbl_spline info. Check previous netcal step!')
    nf  = uvf+'+LMTcal'
    if os.path.exists(nf+'.uvfits'):
        print(f'{nf}.uvfits already exists.')
        polfixed_save(None, nf+'.uvfits')
        return nf, None
    obs = polfixed_uvload(uvf+'.uvfits')
    obs.add_scans(info='vex', filepath=vexfile)
    jcmt = 'JC'
    if 'MM' in obs.tarr['site']:
        jcmt = 'MM'
    obs_nc_JC = obs.flag_sites([jcmt],output='flagged')
    obs_nc = obs.flag_sites([jcmt],output='kept')
    obs_nc_stokes = obs_nc.switch_polrep('stokes')
    obs_split = obs_nc_stokes.split_obs()
    old_stdout  = sys.stdout
    mute_stdout = open(os.devnull, 'w')
    sys.stdout  = mute_stdout
    for j in range(len(obs_split)):
        try:
            zbl = zbl_spline(obs_split[j].data['time'][0])
        except TypeError:
            # Constant total flux density.
            zbl = zbl_spline
        obs_LMT = obs_split[j].flag_uvdist(uv_max=2e9)
        if len(obs_LMT.data) == 0: continue
        if len(obs_LMT.flag_sites(['LM'],output='flagged').data) == 0: continue
        gausspriorLMT = eh.image.make_square(obs_split[j], 32, 150.0*eh.RADPERUAS
                                            ).add_gauss(zbl, (sourcesize*eh.RADPERUAS, sourcesize*eh.RADPERUAS, 0, 0, 0))
        caltab = eh.selfcal(obs_LMT, gausspriorLMT, sites=['LM'], method='amp', ttype='direct', processes=2,
                            caltable=True, gain_tol=1.0)
        obs_split[j] = caltab.applycal(obs_split[j], interp='nearest', extrapolate=True)
    sys.stdout  = old_stdout
    obs_nc_LMc = di.merge_obs(obs_split)
    #obs_nc_LMc = obs_nc_LMc.flag_anomalous(field='amp',robust_nsigma_cut=4)
    #obs_nc_LMc = obs_nc_LMc.flag_anomalous(field='snr',robust_nsigma_cut=4)
    obs_nc_LMc = obs_nc_LMc.switch_polrep('circ')
    obs_nc_LMc.data = obs_nc_LMc.data[(obs_nc_LMc.data['llvis']==obs_nc_LMc.data['llvis'])&(obs_nc_LMc.data['rrvis']==obs_nc_LMc.data['rrvis'])]
    obs_nc_LMc.scans=[]
    obs_nc_JC.scans=[]
    obs_nc_LMc = eh.merge_obs([obs_nc_LMc,obs_nc_JC])
    obs_nc_LMc.add_scans(info='vex',filepath=vexfile)
    polfixed_save(obs_nc_LMc, nf+'.uvfits', uvf+'.uvfits')
    return nf, None


def flag_last_scan_datapoint(obs, flagrange_h=0.0001):
    scans = obs.scans
    times = np.unique(obs.data['time'])
    for sc in scans:
        lt  = times[(np.abs(times - sc[1])).argmin()]
        obs = obs.flag_UT_range(lt-flagrange_h, lt+flagrange_h)
    return obs


# Can add new calibration steps (ordered) here using a complete set of metadata.
all_calib_steps =[
('d', get_mnts, 'Metadata'),
('e', evpa_rot, 'EVPA   '),
('l', dcal    , 'Leakage'),
('s', ampscale, 'Scale  '),
('n', netcal  , 'Network'),
('m', lmtcal  , 'LMT')
]
#TODO: Can we do the calibration without frequency averaging? Then export an additional freq+time avg next to the un-averaged UVFs at the end.
#TODO: Why loss of coherence sometimes and/or number of detections??


for f in data.netcal_and_leakage:
    if not os.path.isfile(f):
        print('File missing: '+f)
        continue
    calibd   = data.netcal_and_leakage[f]
    csteps   = list(calibd.keys())
    nfname   = f
    metadata = None
    vexf     = calibd['v']
    for step in all_calib_steps:
        if step[0] in csteps:
            nfname, metadata = step[1](nfname, calibd[step[0]], metadata, vexf)
            if not nfname:
                nfname = f
        else:
            print(f'No {step[2]} calibration for: {nfname}.uvfits')

    if os.path.exists(nfname+'+10s_avg.uvfits'):
        print(f'{nfname}.uvfits already exists.')
        polfixed_save(None, nfname+'+10s_avg.uvfits')
        continue

    #TODO: The 10s avg and some flagging is best done in Difmap; how does ehtim do the 10s averaging wrong?
    #_obs = polfixed_uvload(nfname+'.uvfits')
    #TODO: The command below will get rid of NaNs but it will also flag JCMT..
    #_obs.data = _obs.data[(_obs.data['llvis']==_obs.data['llvis'])&(_obs.data['rrvis']==_obs.data['rrvis'])]
    #_obs.add_scans(info='vex', filepath=vexf)
    #_obs = _obs.avg_coherent(10)
    # Flag bad last data point after avg (likely from <10s interval data).
    #_obs = flag_last_scan_datapoint(_obs)
    #_obs.save_uvfits(nfname+'+10s_avg.uvfits')
    #put_mnts(nfname+'+10s_avg.uvfits')
