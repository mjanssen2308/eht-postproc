e17datapath = '/home/mjanssen/data_collections/VLBI_data/EHT/2017_apr/ER7-bandcombine'


e17evpa = (0.0, 0.0 + 1j*1.0, 1.0 + 1j*0.0)


e17_const_dterms = {
'LM': {'dr':0.025+0.035j,    'dl':-0.010+0.015j},
'AZ': {'dr':0.028+0.090j,    'dl':-0.035+0.100j},
'PV': {'dr':-0.130+0.035j,   'dl':0.150+0.000j},
'AP': {'dr':-0.0867+0.0296j, 'dl':0.0466+0.0458j},
'JC': {'dr':-0.0009-0.0229j, 'dl':-0.0046+0.0334j},
'SM': {'dr':-0.0173+0.0481j, 'dl':0.0279+0.0400j},
}
e17alo = { 'AA': {'dr':0.0078-0.0261j  ,'dl':-0.0040-0.0282j} } | e17_const_dterms
e17ahi = { 'AA': {'dr':-0.0002-0.030j  ,'dl':-0.0056-0.0392j} } | e17_const_dterms
e17blo = { 'AA': {'dr':0.0060-0.0545j  ,'dl':-0.0053-0.0608j} } | e17_const_dterms
e17bhi = { 'AA': {'dr':-0.0009-0.0152j ,'dl':-0.0075-0.0166j} } | e17_const_dterms
e17clo = { 'AA': {'dr':0.0112-0.0710j  ,'dl':-0.0046-0.0577j} } | e17_const_dterms
e17chi = { 'AA': {'dr':0.0125-0.0493j  ,'dl':-0.0037-0.0400j} } | e17_const_dterms
e17dlo = { 'AA': {'dr':0.0030-0.0280j  ,'dl':-0.0142-0.0374j} } | e17_const_dterms
e17dhi = { 'AA': {'dr':-0.0017-0.0410j ,'dl':-0.0109-0.0402j} } | e17_const_dterms
e17elo = { 'AA': {'dr':-0.0015-0.0633j ,'dl':-0.0080-0.0609j} } | e17_const_dterms
e17ehi = { 'AA': {'dr':-0.0029-0.0519j ,'dl':-0.0076-0.0507j} } | e17_const_dterms


trackA=e17datapath+'/e17a10.vex'
trackB=e17datapath+'/e17b06.vex'
trackC=e17datapath+'/e17c07.vex'
trackD=e17datapath+'/e17d05.vex'
trackE=e17datapath+'/e17e11.vex'


netcal_and_leakage = {
e17datapath+'/trackB/J1733-1304_calibrated.uvf.spw0to31':  {'l':e17blo, 'e':e17evpa, 'n':3.18},
e17datapath+'/trackB/J1733-1304_calibrated.uvf.spw32to63': {'l':e17bhi, 'e':e17evpa, 'n':3.16},
e17datapath+'/trackC/J1733-1304_calibrated.uvf.spw0to31':  {'l':e17clo, 'e':e17evpa, 'n':3.11},
e17datapath+'/trackC/J1733-1304_calibrated.uvf.spw32to63': {'l':e17chi, 'e':e17evpa, 'n':3.09},
e17datapath+'/trackD/J1733-1304_calibrated.uvf.spw0to31':  {'l':e17dlo, 'e':e17evpa, 'n':3.18},
e17datapath+'/trackD/J1733-1304_calibrated.uvf.spw32to63': {'l':e17dhi, 'e':e17evpa, 'n':3.16},

e17datapath+'/trackA/M87_calibrated.uvf.spw0to31':         {'l':e17alo, 'e':e17evpa, 'n':1.29},
e17datapath+'/trackA/M87_calibrated.uvf.spw32to63':        {'l':e17ahi, 'e':e17evpa, 'n':1.27},
e17datapath+'/trackB/M87_calibrated.uvf.spw0to31':         {'l':e17blo, 'e':e17evpa, 'n':1.27},
e17datapath+'/trackB/M87_calibrated.uvf.spw32to63':        {'l':e17bhi, 'e':e17evpa, 'n':1.26},
e17datapath+'/trackD/M87_calibrated.uvf.spw0to31':         {'l':e17dlo, 'e':e17evpa, 'n':1.24},
e17datapath+'/trackD/M87_calibrated.uvf.spw32to63':        {'l':e17dhi, 'e':e17evpa, 'n':1.23},
e17datapath+'/trackE/M87_calibrated.uvf.spw0to31':         {'l':e17elo, 'e':e17evpa, 'n':1.29},
e17datapath+'/trackE/M87_calibrated.uvf.spw32to63':        {'l':e17ehi, 'e':e17evpa, 'n':1.28},

e17datapath+'/trackA/OJ287_calibrated.uvf.spw0to31':       {'l':e17alo, 'e':e17evpa, 'n':4.12},
e17datapath+'/trackA/OJ287_calibrated.uvf.spw32to63':      {'l':e17ahi, 'e':e17evpa, 'n':4.09},
e17datapath+'/trackD/OJ287_calibrated.uvf.spw0to31':       {'l':e17dlo, 'e':e17evpa, 'n':4.22},
e17datapath+'/trackD/OJ287_calibrated.uvf.spw32to63':      {'l':e17dhi, 'e':e17evpa, 'n':4.19},
e17datapath+'/trackE/OJ287_calibrated.uvf.spw0to31':       {'l':e17elo, 'e':e17evpa, 'n':4.17},
e17datapath+'/trackE/OJ287_calibrated.uvf.spw32to63':      {'l':e17ehi, 'e':e17evpa, 'n':4.15},

e17datapath+'/trackA/CENA_calibrated.uvf.spw0to31':        {'l':e17alo, 'e':e17evpa, 'n':5.62},
e17datapath+'/trackA/CENA_calibrated.uvf.spw32to63':       {'l':e17ahi, 'e':e17evpa, 'n':5.62},

e17datapath+'/trackB/3C273_calibrated.uvf.spw0to31':       {'l':e17blo, 'e':e17evpa, 'n':7.42},
e17datapath+'/trackB/3C273_calibrated.uvf.spw32to63':      {'l':e17bhi, 'e':e17evpa, 'n':7.35},

e17datapath+'/trackB/SGRA_calibrated.uvf.spw0to31':        {'l':e17blo, 'e':e17evpa, 'n':{'AA': e17datapath+'/LCs/e17b06lo_AALC.dat', 'SM':e17datapath+'/LCs/e17b06lo_SMLC.dat'}, 'm':60.},
e17datapath+'/trackB/SGRA_calibrated.uvf.spw32to63':       {'l':e17bhi, 'e':e17evpa, 'n':{'AA': e17datapath+'/LCs/e17b06hi_AALC.dat', 'SM':e17datapath+'/LCs/e17b06hi_SMLC.dat'}, 'm':60.},
e17datapath+'/trackC/SGRA_calibrated.uvf.spw0to31':        {'l':e17clo, 'e':e17evpa, 'n':{'AA': e17datapath+'/LCs/e17c07lo_AALC.dat', 'SM':e17datapath+'/LCs/e17c07lo_SMLC.dat'}, 'm':60.},
e17datapath+'/trackC/SGRA_calibrated.uvf.spw32to63':       {'l':e17chi, 'e':e17evpa, 'n':{'AA': e17datapath+'/LCs/e17c07hi_AALC.dat', 'SM':e17datapath+'/LCs/e17c07hi_SMLC.dat'}, 'm':60.},
e17datapath+'/trackE/SGRA_calibrated.uvf.spw0to31':        {'l':e17elo, 'e':e17evpa, 'n':{'AA': e17datapath+'/LCs/e17e11lo_AALC.dat', 'SM':e17datapath+'/LCs/e17e11lo_SMLC.dat'}, 'm':60.},
e17datapath+'/trackE/SGRA_calibrated.uvf.spw32to63':       {'l':e17ehi, 'e':e17evpa, 'n':{'AA': e17datapath+'/LCs/e17e11hi_AALC.dat', 'SM':e17datapath+'/LCs/e17e11hi_SMLC.dat'}, 'm':60.},
}

AAscale = {'AA': 1.07} # https://eventhorizontelescope.teamwork.com/app/notebooks/206732
for f in netcal_and_leakage:
    netcal_and_leakage[f]['d'] = f
    netcal_and_leakage[f]['s'] = AAscale
    if 'trackA' in f:
        netcal_and_leakage[f]['v'] = trackA
    elif 'trackB' in f:
        netcal_and_leakage[f]['v'] = trackB
    elif 'trackC' in f:
        netcal_and_leakage[f]['v'] = trackC
    elif 'trackD' in f:
        netcal_and_leakage[f]['v'] = trackD
    elif 'trackE' in f:
        netcal_and_leakage[f]['v'] = trackE
